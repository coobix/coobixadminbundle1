<?php

namespace VendorName\BundleName\Admin\Entity;

use Coobix\AdminBundle\Entity\Admin;
use VendorName\BundleName\Form\EntityNameType as NewType;
use VendorName\BundleName\Form\EntityNameEditType as EditType;
use VendorName\BundleName\Admin\Form\EntityNameListSearchType as ListSearchType;

class EntityNameAdmin extends Admin {

  public function getNewTitle() {
        return "Crear nueva EntityName";
    }

    public function getEditTitle() {
        return "Editar EntityName";
    }

    public function __construct($class) {
        $this->setListSearchForm(ListSearchType::class);
        $this->setNewForm(NewType::class);
        $this->setEditForm(EditType::class);
        
        $this->setListTemplate('VendorNameBundleName:EntityName:Admin/list.html.twig');
        $this->setShowTemplate('VendorNameBundleName:EntityName:Admin/show.html.twig');
        parent::__construct($class);
    }

}
