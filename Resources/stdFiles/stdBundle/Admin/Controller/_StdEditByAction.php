
    public function editByEntity2NameAction($id) {
        $admin = $this->get("coobix.admin.entity1name");

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);

        if (!$entity) {
            $rUrl = $this->generateUrl("admin");
            return $this->forward('CoobixAdminBundle:Admin:notFoundRedirect', array(
                        'url' => $rUrl,
            ));
        }

        $form = $this->createForm(Entity1NameByEntity2NameType::class, $entity, array(
            'action' => $this->generateUrl('entity1name_updatebyentity2name_admin', array('id' => $entity->getId())),
            'method' => 'POST',
        ));
        $form->add('apply', SubmitType::class, array('label' => 'APLICAR'));
        $form->add('save', SubmitType::class, array('label' => 'MODIFICAR'));

        $formOptions = array(
            'id' => $id,
            'label' => 'Borrar',
            'action' => $this->generateUrl("entity1name_deletebyentity2name_admin", array('id' => $id))
        );
        $deleteForm = $this->createDeleteForm($admin, $formOptions);

        $template = $admin->getBundleName() . ':' . $admin->getClassName() . ':Admin/edit_by_entity2name.html.twig';
        return $this->render($template, array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'deleteForm' => $deleteForm->createView(),
        ));
    }
