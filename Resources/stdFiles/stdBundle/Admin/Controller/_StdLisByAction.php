    public function listByEntity2NameAction(Request $request, $entity2nameId) {

        $admin = $this->get("coobix.admin.entity1name");

        $em = $this->getDoctrine()->getManager();

        $relEntity = $em->getRepository('Vendor2NameBundle2Name:Entity2Name')->find($entity2nameId);
        
        if (!$relEntity) {
            $rUrl = $this->generateUrl("admin");
            return $this->forward('CoobixAdminBundle:Admin:notFoundRedirect', array(
                        'url' => $rUrl,
            ));
        }

        //CONSULTA PARA RETORNAR LAS ENTIDADES DE LA PAGINA
        //A ESTA CONSULTA LUEGO SE LE AGREGAN FILTROS DE ORDEN, PAGINADO, ETC
        $qb = $em->createQueryBuilder();
        $qb->select('e')->from($admin->getBundleName() . ':' . $admin->getClassName(), 'e');

        $qb->andWhere('e.entity2Name = :e_entity2Name');
        //$qb->andWhere('e.empresa = :e_empresa');
        $qb->setParameter('e_entity2Name', $relEntity);
        $qb->orderBy('e.createdAt', 'DESC');

        $list = new BaseList($this->getDoctrine(), $admin->getClass());
        $list->setQb($qb);

        $listSearchForm = $this->createForm($admin->getListSearchForm(), null, array(
            'action' => $this->generateUrl("entity1name_listbyentity2name_admin", array('entity2nameId' => $relEntity->getId())),
            'method' => 'GET',
        ));


        //$listSearchForm = $this->createSearchListForm($admin);
        $listSearchForm->handleRequest($request);
        $listSearchFormIsSubmited = false;
        if ($listSearchForm->isSubmitted()) {
            $listSearchFormIsSubmited = true;
            if ($listSearchForm->isValid()) {
                $list->applyFilters();
            }
        }

        $list->applyOrder();

        //PAGINADOR
        //Clono el query builder para el paginador
        $qbp = clone($list->getQb());
        $qbp->select('count(e.id)');
        $entitiesCount = $qbp->getQuery()->getSingleResult();

        $pager = $this->get('coobix.pager');
        //Url en la cual trabaja el paginador
        $listUrlParams = array(
            'routeName' => "entity1name_listbyentity2name_admin",
            'parameters' => array('class' => strtolower($admin->getClassName()), 'entity2nameId' => $relEntity->getId())
        );
        //LA URL DEBERIA SER PASADA DIRECTAMENTE Y NO PARA QUE LA HAGA
        //EL PAGINADOR
        $pager->setListUrlParams($listUrlParams);
        $pager->setNumItems($entitiesCount[1]);
        $pager->configure();

        $list->applyLimits();
        $list->getResult();

        $template = $admin->getClassName() . '/Admin/list_by_entity2name.html.twig';
        return $this->render($template, array(
                    'admin' => $admin,
                    'listSearchFormIsSubmited' => $listSearchFormIsSubmited,
                    'listSearchForm' => $listSearchForm->createView(),
                    'list' => $list,
                    'pager' => $pager,
                    'entity2Name' => $relEntity
        ));
    }
