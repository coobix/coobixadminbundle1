
    public function showByEntity2NameAction($id) {
        $admin = $this->get("coobix.admin.entity1name");

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);
        
        if (!$entity) {
            $rUrl = $this->generateUrl("admin");
            return $this->forward('CoobixAdminBundle:Admin:notFoundRedirect', array(
                        'url' => $rUrl,
            ));
        }

        $formOptions = array(
            'id' => $id,
            'label' => 'Borrar',
            'action' => $this->generateUrl("entity1name_deletebyentity2name_admin", array('id' => $id)
        ));
        $deleteForm = $this->createDeleteForm($admin, $formOptions);

        $template = $admin->getClassName() . '/Admin/show_by_entity2name.html.twig';
        return $this->render($template, array(
                    'entity' => $entity,
                    'admin' => $admin,
                    'deleteForm' => $deleteForm->createView(),
        ));
    }
