  
    public function createByEntity2NameAction(Request $request) {
        
        $admin = $this->get("coobix.admin.entity1name");
        $em = $this->getDoctrine()->getManager();
        $entity2nameId = $request->request->get('sgt_appbundle_tarifa[entity2name]', null, true);
        
        $relEntity = $em->getRepository('Vendor2NameBundle2Name:Entity2Name')->find($entity2nameId);
        
        $c = $admin->getClass();
        $entity = new $c;
        $entity->setEntity2Name($relEntity);

        $form = $this->createForm(Entity1NameByEntity2NameType::class, $entity, array(
            'action' => $this->generateUrl('entity1name_createbyentity2name_admin'),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'CREAR'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            //LOG
            $this->get('session')->getFlashBag()->add('success', 'REGISTRO CREADO.');
            $this->get('coobix.log')->create('CREATE ' . $admin->getClassName() . ': ' . $entity . '. ID: ' . $entity->getId());
            return $this->redirect($this->generateUrl('entity1name_showbyentity2name_admin', array('id' => $entity->getId())));
        }

        $this->get('session')->getFlashBag()->add('danger', 'EL REGISTRO NO SE PUDO CREAR VERIFIQUE EL FORMULARIO.');

        $template = $admin->getClassName() . '/Admin/new_by_entity2name.html.twig';
        return $this->render($template, array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }
