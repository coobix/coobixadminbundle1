
    public function bulkByEntity2NameAction(Request $request) {
        $admin = $this->get("coobix.admin.entity1name");
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('bulkIds'));
        unset($ids[0]);

        $entity = $em->getRepository($admin->getEntityShortcutName())->find($ids[1]);
        $urlReturn = $this->generateUrl("entity1name_listbyentity2name_admin", array('entity2nameId' => $entity->getEntity2Name()->getId()));


        $action = $request->request->get('action');

        switch ($action) {
            case 'delete':
                $em = $this->getDoctrine()->getManager();

                //LE PASO LOS PARAMETROS ASÍ POR QUE DE LA OTRA FORMA LOS MANDA COMO STRING '9,10'
                //Y NO FUNCIONA
                $q = $em->createQuery(' SELECT e FROM ' . $admin->getEntityShortcutName() . ' e '
                        . ' WHERE e.id IN (:e_ids) ');
                $q->setParameter('e_ids', $ids);
                $entities = $q->getResult();

                $result = $this->deleteBulk($admin, $entities);

                if ($result['success'] > 0) {
                    $this->get('session')->getFlashBag()->add('success', 'SE ELIMINARON ' . $result['success'] . ' REGISTROS.');
                }
                if ($result['error'] > 0) {
                    $this->get('session')->getFlashBag()->add('danger', 'NO SE PUDIERON ELIMINAR ' . $result['error'] . ' REGISTROS.');
                }

                break;
            default :
        }

        return $this->redirect($urlReturn);
    }
