
    public function listDeleteFormByEntity2NameAction($id) {
        $admin = $this->get("coobix.admin.entity1name");

        $formOptions = array('id' => $id,
            'action' =>
            $this->generateUrl("entity1name_deletebyentity2name_admin", array('id' => $id)),
        );
        $deleteForm = $this->createDeleteForm($admin, $formOptions);

        return $this->render('CoobixAdminBundle:Admin:list_delete_form.html.twig', array(
                    'delete_form' => $deleteForm->createView(),
        ));
    }
