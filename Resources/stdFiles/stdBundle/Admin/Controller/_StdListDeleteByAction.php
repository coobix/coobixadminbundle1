
    public function deleteByEntity2NameAction(Request $request, $id) {
        $admin = $this->get("coobix.admin.entity1name");

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);
        $returnUrl = $this->generateUrl("entity1name_listbyentity2name_admin", array('entity2nameId' => $entity->getEntity2Name()->getId()));

        $formOptions = array('id' => $id);
        $form = $this->createDeleteForm($admin, $formOptions);

        $form->handleRequest($request);

        if ($form->isValid()) {
            if (!$entity) {
                return $this->forward('CoobixAdminBundle:Admin:notFoundRedirect', array(
                            'url' => $returnUrl,
                ));
            }

            try {
                $em->remove($entity);

                $msg = 'DELETE ' . $admin->getClassName() . ': ' . $entity . '. ID: ' . $entity->getId();
                $em->flush();

                //LOG
                $this->get('coobix.log')->create($msg);

                //MSJ FLASH
                $this->get('session')->getFlashBag()->add('success', 'REGISTRO ELIMINADO.');
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('danger', 'EL REGISTRO NO SE PUDO ELIMINAR DEBIDO A QUE TIENE DATOS ASOCIADOS.');
            }
        }

        return $this->redirect($returnUrl);
    }
