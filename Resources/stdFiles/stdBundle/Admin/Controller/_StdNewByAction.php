
    public function newByEntity2NameAction(Request $request, $entity2nameId) {

        $admin = $this->get("coobix.admin.entity1name");

        $em = $this->getDoctrine()->getManager();

        $relEntity = $em->getRepository('Vendor2NameBundle2Name:Entity2Name')->find($entity2nameId);

        $c = $admin->getClass();
        $entity = new $c;
        $entity->setEntity2Name($relEntity);

        $form = $this->createForm(Entity1NameByEntity2NameType::class, $entity, array(
            'action' => $this->generateUrl('entity1name_createbyentity2name_admin'),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'CREAR'));

        $template = $admin->getClassName() . '/Admin/new_by_entity2name.html.twig';

        return $this->render($template, array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }
    
