
    /**
     * Edits an existing Entity1Name class.
     *
     */
    public function updateByEntity2NameAction(Request $request, $id) {
        $admin = $this->get("coobix.admin.entity1name");

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);

        if (!$entity) {
            $rUrl = $this->generateUrl("admin");
            return $this->forward('CoobixAdminBundle:Admin:notFoundRedirect', array(
                        'url' => $rUrl,
            ));
        }

        $form = $this->createForm(Entity1NameByEntity2NameType::class, $entity, array(
            'action' => $this->generateUrl('entity1name_updatebyentity2name_admin', array('id' => $entity->getId())),
            'method' => 'POST',
        ));
        $form->add('apply', SubmitType::class, array('label' => 'APLICAR'));
        $form->add('save', SubmitType::class, array('label' => 'MODIFICAR'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
            //LOG
            $this->get('coobix.log')->create('UPDATE ' . $admin->getClassName() . ': ' . $entity . '. ID: ' . $entity->getId());
            $this->get('session')->getFlashBag()->add('success', 'REGISTRO MODIFICADO.');

            if ($form->get('save')->isClicked()) {
                return $this->redirect($this->generateUrl('entity1name_showbyentity2name_admin', array('id' => $entity->getId())));
            }

            return $this->redirect($this->generateUrl('entity1name_editbyentity2name_admin', array('id' => $entity->getId())));
        }

        $formOptions = array(
            'id' => $id,
            'label' => 'Borrar'
        );
        $deleteForm = $this->createDeleteForm($admin, $formOptions);

        $this->get('session')->getFlashBag()->add('danger', 'EL REGISTRO NO SE PUDO MODIFICAR VERIFIQUE EL FORMULARIO.');

        $template = $admin->getBundleName() . ':' . $admin->getClassName() . ':Admin/edit_by_entity2name.html.twig';
        return $this->render($template, array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'deleteForm' => $deleteForm->createView(),
        ));
    }
