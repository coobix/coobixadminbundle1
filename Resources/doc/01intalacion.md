Instalación de CoobixAdminBundle
==================================

## Prerequisitos

## Instalación

1. Descargar CoobixAdminBundle usando composer
2. Habilitar el bundle


### Paso 1: Descargar CoobixAdminBundle usando composer

Agregar el repositorio del bundle en composer.json:

``` bash
"repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:coobix/coobixadminbundle.git"
        }
    ]
```

Agregar el bundle con el siguiente comando:

``` bash
$ composer require coobix/admin-bundle "dev-master"
```

Composer instalara el  bundle en el directorio `vendor/coobix`.

### Paso 2: Habilitar el bundle

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Coobix\AdminBundle\CoobixAdminBundle(),
    );
}
```



### Paso 4: Configurar el bundle

# TODO: Buscar la forma de que esto lo tenga el bundle y no el config.yml

``` yaml
# app/config/config.yml
# Twig Configuration
twig:
    form_themes:
        - 'CoobixAdminBundle:Form:sf3_form_div_layout.html.twig'
    globals:
        uploads_dir: "uploads/"
        images_dir: "uploads/images/"
        locale: %locale%
        novalidate: ''

```


### Paso 5: Crear carpetas


``` bash

$ mkdir uploads
$ mkdir uploads/images
$ mkdir images
```


YAML:

``` yaml
# app/config/routing.yml
fos_js_routing:
    resource: "@FOSJsRoutingBundle/Resources/config/routing/routing.xml"
```

```
$ php bin/console assets:install --symlink web
```


