Crear una entidad
==================================

#### Generar entidad

### Paso 1: Generar entidad

``` bash
$ php bin/console doctrine:generate:entity
```

``` bash
The Entity shortcut name: AppBundle:Test

Configuration format (yml, xml, php, or annotation) [annotation]: ENTER

Do you want to generate an empty repository class [no]? yes
```

## Agregar en la Entidad:

``` php
use Symfony\Component\Validator\Constraints as Assert;

 * @ORM\HasLifecycleCallbacks()

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Assert\DateTime()
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     * @Assert\DateTime()
     */
    private $updatedAt;

   /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
    	$this->setCreatedAt (new \DateTime);
    	$this->setUpdatedAt (new \DateTime);
    }
    
    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
    	$this->setUpdatedAt (new \DateTime);
    }

    public function __toString() {
        return '';
    }
```

## Genero seters y geters

``` bash
$ php bin/console doctrine:generate:entities App
```

## Actualizo la base de datos

``` bash
$ php bin/console doctrine:schema:update --force
```

### Paso 2: Generar forms de la entidad

``` bash
$ php bin/console generate:doctrine:form AppBundle:Test
```

## Crear el formulario de edit

. Borrar en el archivo EntidadType: 
    ->add('createdAt')
    ->add('updatedAd')

. Copiar el EntidadType y pegarlo como EntidadEditType

. Cambiar el nombre de la clase en el EntidadEditType


