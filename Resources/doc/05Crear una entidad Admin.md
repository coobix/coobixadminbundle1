Crear una entidad Admin
==================================

### Paso 1:
Ir a 
/project/src/VENDOR/AdminBundle/Resources/views/Admin/sidebar.html.twig

Habilitar el menu DEV.

### Paso 2:

En opción Generar CRUD Admin:
Completar el formulario.

PROBLEMA DE PERMISOS:

``` bash
$ sudo chown -R www-data src/VENDOR/AppBundle/
```

``` bash
$ sudo chown -R nicolay src/VENDOR/AppBundle/
```

### Paso 4: Agregar servicio de entidad Admin

En app/config/config.yml agregar
services:
    coobix.admin.entidad:
        class: Vendor\EntidadBundle\Admin\Entity\EntidadAdmin
        arguments:
            - Vendor\EntidadBundle\Entity\Entidad
            - @service_container