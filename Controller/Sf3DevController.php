<?php

namespace Coobix\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Coobix\AdminBundle\Controller\BaseDevController as BaseController;
use Symfony\Component\Form\Extension\Core\Type;

/**
 * Dev controller.
 */
class Sf3DevController extends BaseController
{
    /**
     * *********
     * CRUD BY *
     * *********
     */

    /**
     * Display a form to do Admin CRUD's BY tasks.
     */
    public function adminCrudByNewAction() {

        $form = $this->createAdminCrudByCreateForm();
        return $this->render('CoobixAdminBundle:Dev:newByCrud.html.twig', array(
                    'form' => $form->createView(),
                    'serviceConfig' => '',
                    'tasks' => array(),
        ));
    }

    /**
     * Creates a form to create a CRUD entity.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createAdminCrudByCreateForm($defaultData = array()) {

        $form = $this->createFormBuilder($defaultData)
                ->setAction($this->generateUrl('dev_crudbycreate'))
                ->setMethod('GET')
                ->add('entity1ShortcutName', Type\TextType::class, array('label' => 'Entidad Prin: Entity Shortcut Name (Acme/TestBundle:Test)'))
                ->add('entity2ShortcutName', Type\TextType::class, array('label' => 'Entidad Rel.: Entity Shortcut Name (Acme/TestBundle:Test)'))
                ->add('tasks', Type\ChoiceType::class, array(
                    'choices' => array(
                          'Generar ListBy Action' => 'gen-listby-action',
                          'Generar ListBy Route'=> 'gen-listby-route',
                          'Generar ListBy Template'=> 'gen-listby-template',
                          'Generar NewBy Action'=> 'gen-newby-action',
                          'Generar NewBy Route'=> 'gen-newby-route',
                          'Generar NewBy Template'=> 'gen-newby-template',
                          'Generar CreateBy Action'=> 'gen-createby-action',
                          'Generar CreateBy Route'=> 'gen-createby-route',
                          'Generar EditBy Action'=> 'gen-editby-action',
                          'Generar EditBy Route'=> 'gen-editby-route',
                          'Generar EditBy Template'=> 'gen-editby-template',
                          'Generar UpdateBy Action'=> 'gen-updateby-action',
                          'Generar UpdateBy Route'=> 'gen-updateby-route',
                          'Generar ShowBy Action'=> 'gen-showby-action',
                          'Generar ShowBy Route'=> 'gen-showby-route',
                          'Generar ShowBy Template'=> 'gen-showby-template',
                          'Generar ListDeleteBy Action'=> 'gen-listdeleteby-action',
                          'Generar DeleteBy Route'=> 'gen-deleteby-route',
                          'Generar ListDeleteFormBy Action'=> 'gen-listdeleteformby-action',
                          'Generar ListDeleteFormBy Route'=> 'gen-listdeleteformby-route',
                          'Generar bulkBy Action'=> 'gen-bulkby-action',
                          'Generar BulkBy Route'=> 'gen-bulkby-route',
                    ),
                    'expanded' => true,
                    'multiple' => true,
                    'attr' => array('class' => 'form-group-checkbox')
                ))
                ->add('submit', Type\SubmitType::class, array('label' => 'Crear'))
                ->getForm();
        return $form;
    }

    /**
     * Execute selected Admin CRUD's tasks.
     *
     */
    public function adminCrudByCreateAction(Request $request) {

        $form = $this->createAdminCrudByCreateForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $formData = $form->getData();

            $tasks = array();

            if (array_search('gen-listby-action', $formData['tasks']) !== false) {
                $tasks[] = $this->generateListByMethod($formData);
            }
            if (array_search('gen-listby-template', $formData['tasks']) !== false) {
                $tasks[] = $this->generateListByTemplate($formData);
            }
            if (array_search('gen-newby-action', $formData['tasks']) !== false) {
                $tasks[] = $this->generateNewByMethod($formData);
            }
            if (array_search('gen-newby-template', $formData['tasks']) !== false) {
                $tasks[] = $this->generateNewByTemplate($formData);
            }
            if (array_search('gen-createby-action', $formData['tasks']) !== false) {
                $tasks[] = $this->generateCreateByMethod($formData);
            }
            if (array_search('gen-showby-action', $formData['tasks']) !== false) {
                $tasks[] = $this->generateShowByMethod($formData);
            }
            if (array_search('gen-showby-template', $formData['tasks']) !== false) {
                $tasks[] = $this->generateShowByTemplate($formData);
            }
            if (array_search('gen-editby-action', $formData['tasks']) !== false) {
                $tasks[] = $this->generateEditByMethod($formData);
            }
            if (array_search('gen-editby-template', $formData['tasks']) !== false) {
                $tasks[] = $this->generateEditByTemplate($formData);
            }
            if (array_search('gen-updateby-action', $formData['tasks']) !== false) {
                $tasks[] = $this->generateUpdateByMethod($formData);
            }
            if (array_search('gen-listdeleteformby-action', $formData['tasks']) !== false) {
                $tasks[] = $this->generateListDeleteByFormMethod($formData);
            }
            if (array_search('gen-listdeleteby-action', $formData['tasks']) !== false) {
                $tasks[] = $this->generateListDeleteByMethod($formData);
            }
            if (array_search('gen-bulkby-action', $formData['tasks']) !== false) {
                $tasks[] = $this->generateBulkByMethod($formData);
            }

            //Rutas
            $tasks[] = $this->generateActionByRoute($formData);


            $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
            $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

            $serviceConfig = ''
                    . '<pre>'
                    . '//config.yml o src/' . ($entity1Data['vendorName'] ? $entity1Data['vendorName'] . '/' : '') . $entity1Data['bundleName'] . '/Resources/config/services.yml<br>'
                    . 'services:<br>'
                    . '    coobix.admin.' . strtolower($entity1Data['className']) . ':<br>'
                    . '        class: ' . ($entity1Data['vendorName'] ? $entity1Data['vendorName'] . '\\' : '') . $entity1Data['bundleName'] . '\Admin\Entity\\' . $entity1Data['className'] . 'Admin<br>'
                    . '        arguments:<br>'
                    . '            - ' . ($entity1Data['vendorName'] ? $entity1Data['vendorName'] . '\\' : '') . $entity1Data['bundleName'] . '\Entity\\' . $entity1Data['className'] . '<br>'
                    . '</pre>';

            //Config del routing
            $serviceConfig .= ''
                    . '<pre>'
                    . '//src/' . $entity1Data['className'] . '/' . $entity1Data['bundleName'] . '/Resources/config/routing.yml<br>'
                    . $entity1Data['vendorName'] . $entity1Data['bundleName'] . '_' . strtolower($entity1Data['className']) . '_admin:<br>'
                    . '    resource: "@' . $entity1Data['vendorName'] . $entity1Data['bundleName'] . '/Resources/config/routing/' . strtolower($entity1Data['className']) . '_admin.yml"<br>'
                    . '    prefix:   /admin<br>'
                    . '</pre>';
        } else {
            echo "error";
            $tasks = array();
            $serviceConfig = '';
        }

        return $this->render('CoobixAdminBundle:Dev:newCrud.html.twig', array(
                    'form' => $form->createView(),
                    'serviceConfig' => $serviceConfig,
                    'tasks' => $tasks,
        ));
    }

    public function generateListByMethod($formData) {
        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo Destino
        $destFile = $entity1Data['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] . 'AdminController.php';

        //Me fijo si existe el archivo EntityAdminController
        if (!file_exists($destFile)) {
            throw $this->createNotFoundException('No existe el archivo ' . $destFile);
        }
        //Leo el archivo para buscar el string //methods
        try {
            $f = fopen($destFile, 'r');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $str = fread($f, filesize($destFile));
        fclose($f);
        if (FALSE === strrpos($str, '//methods')) {
            throw $this->createNotFoundException('No se encontré el string "//methods" para poder escribir el método. '
                    . 'Agregar //methods donde se va a escribir el método.');
        }

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . '_StdLisByAction.php';

        //Leo archivo STD
        $f1 = fopen($resourceFile, 'r');
        $str1 = fread($f1, filesize($resourceFile));
        fclose($f1);

        //Reemplazo Strings
        $str1 = str_replace('Vendor1Name', $entity1Data['vendorName'], $str1);
        $str1 = str_replace('Bundle1Name', $entity1Data['bundleName'], $str1);
        $str1 = str_replace('Entity1Name', $entity1Data['className'], $str1);
        $str1 = str_replace('entity1name', strtolower($entity1Data['className']), $str1);
        $str1 = str_replace('Vendor2Name', $entity2Data['vendorName'], $str1);
        $str1 = str_replace('Bundle2Name', $entity2Data['bundleName'], $str1);
        $str1 = str_replace('Entity2Name', $entity2Data['className'], $str1);
        $str1 = str_replace('entity2name', strtolower($entity2Data['className']), $str1);
        $str1 = str_replace('entity2Name', lcfirst($entity2Data['className']), $str1);

        $str = str_replace('//methods', $str1 . chr(13) . chr(13) . '//methods', $str);
        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    private function generateListByTemplate($formData) {

        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'std_list_by.html.twig';

        //Archivo Destino
        if ($entity1Data['vendorName']) {
            $destFile = $entity1Data['bundleDir'];
        } else {
            $destFile = $this->container->getParameter('kernel.root_dir');
        }
        $destFile .= DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'list_by_' . strtolower($entity2Data['className']) . '.html.twig'
        ;

        //Reemplazo Strings
        $f = fopen($resourceFile, 'r');
        $str = fread($f, filesize($resourceFile));
        fclose($f);

        // Acme/TestBundle:Test
        if ($entity1Data['vendorName']) {
            $classNameSpace = "\\" . $entity1Data['vendorName'] . "\\" . $entity1Data['bundleName'] . "\\Entity\\" . $entity1Data['className'];
        }
        // Acme/TestBundle:Test
        else {
            $classNameSpace = "\\" . $entity1Data['bundleName'] . "\\Entity\\" . $entity1Data['className'];
        }

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        $metaData = $em->getClassMetadata($classNameSpace);

        $fieldNames = $metaData->getFieldNames();
        foreach ($fieldNames as $f) {

            $field = $metaData->getFieldMapping($f);
            if ($field['fieldName'] == 'id') {
                continue;
            }


            /*
              Array (
              [fieldName] => tipoResolucion
              [type] => integer
              [scale] => 0
              [length] =>
              [unique] =>
              [nullable] =>
              [precision] => 0
              [columnName] => tipoResolucion
              )
             */


            switch ($field['type']) {
                default :
                    $str = str_replace('{# Col-title #}', ''
                            . '<th><a href="{{ list.getColFilterUrl(\'' . $field['fieldName'] . '\') }}">' . ucfirst($field['fieldName']) . '</a></th>'
                            . chr(13) . chr(9) . chr(9) . '{# Col-title #}', $str);
                    break;
            }
        }

        $associationMappings = $metaData->getAssociationMappings();
        foreach ($associationMappings as $field) {
            $str = str_replace('{# Col-title #}', ''
                    . '<th><a href="{{ list.getColFilterUrl(\'' . $field['fieldName'] . '\') }}">' . ucfirst($field['fieldName']) . '</a></th>'
                    . chr(13) . chr(9) . chr(9) . '{# Col-title #}', $str);
            break;
        }

        //Reemplazo la parte de los valores
        foreach ($fieldNames as $f) {
            $field = $metaData->getFieldMapping($f);

            if ($field['fieldName'] == 'id') {
                continue;
            }

            switch ($field['type']) {
                case 'datetime':
                    $str = str_replace('{# Col-value #}', ''
                            . '<td><a href="{{ path(\'entity1name_showbyentity2name_admin\', routeParams) }}">'
                            . '{{ e.get' . ucfirst($field['fieldName']) . '() | date("d/m/Y H:i:s") }}</a></td>'
                            . chr(13) . chr(9) . chr(9) . '{# Col-value #}', $str);
                    break;
                case 'date':
                    $str = str_replace('{# Col-value #}', ''
                            . '<td><a href="{{ path(\'entity1name_showbyentity2name_admin\', routeParams) }}">'
                            . '{{ e.get' . ucfirst($field['fieldName']) . '() | date("d/m/Y") }}</a></td>'
                            . chr(13) . chr(9) . chr(9) . '{# Col-value #}', $str);
                    break;
                default :
                    $str = str_replace('{# Col-value #}', ''
                            . '<td><a href="{{ path(\'entity1name_showbyentity2name_admin\', routeParams) }}">'
                            . '{{ e.get' . ucfirst($field['fieldName']) . '() }}</a></td>'
                            . chr(13) . chr(9) . chr(9) . '{# Col-value #}', $str);
                    break;
            }
        }

        $associationMappings = $metaData->getAssociationMappings();
        foreach ($associationMappings as $field) {
            $str = str_replace('{# Col-value #}', ''
                    . '<td><a href="{{ path(\'entity1name_showbyentity2name_admin\', routeParams) }}">'
                    . '{{ e.get' . ucfirst($field['fieldName']) . '() }}</a></td>'
                    . chr(13) . chr(9) . chr(9) . '{# Col-value #}', $str);
            break;
        }

        //El numero de columnas de la tabla
        $numCols = count($fieldNames) + count($associationMappings);
        $str = str_replace('{# num-cols #}', $numCols, $str);

        $str = str_replace('Entity1Name', $entity1Data['className'], $str);
        $str = str_replace('Entity2Name', $entity2Data['className'], $str);
        $str = str_replace('entity1name', strtolower($entity1Data['className']), $str);
        $str = str_replace('entity2name', strtolower($entity2Data['className']), $str);
        $str = str_replace('entity2Name', lcfirst($entity2Data['className']), $str);


        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateNewByMethod($formData) {
        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo Destino
        $destFile = $entity1Data['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] . 'AdminController.php';

        //Me fijo si existe el archivo EntityAdminController
        if (!file_exists($destFile)) {
            throw $this->createNotFoundException('No existe el archivo ' . $destFile);
        }
        //Leo el archivo para buscar el string //methods
        try {
            $f = fopen($destFile, 'r');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $str = fread($f, filesize($destFile));
        fclose($f);
        if (FALSE === strrpos($str, '//methods')) {
            throw $this->createNotFoundException('No se encontré el string "//methods" para poder escribir el método. '
                    . 'Agregar //methods donde se va a escribir el método.');
        }

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . '_StdNewByAction.php';

        //Leo archivo STD
        $f1 = fopen($resourceFile, 'r');
        $str1 = fread($f1, filesize($resourceFile));
        fclose($f1);

        //Reemplazo Strings
        $str1 = str_replace('Vendor1Name', $entity1Data['vendorName'], $str1);
        $str1 = str_replace('Bundle1Name', $entity1Data['bundleName'], $str1);
        $str1 = str_replace('Entity1Name', $entity1Data['className'], $str1);
        $str1 = str_replace('entity1name', strtolower($entity1Data['className']), $str1);
        $str1 = str_replace('Vendor2Name', $entity2Data['vendorName'], $str1);
        $str1 = str_replace('Bundle2Name', $entity2Data['bundleName'], $str1);
        $str1 = str_replace('Entity2Name', $entity2Data['className'], $str1);
        $str1 = str_replace('entity2name', strtolower($entity2Data['className']), $str1);
        $str1 = str_replace('entity2Name', lcfirst($entity2Data['className']), $str1);

        $str = str_replace('//methods', $str1 . chr(13) . chr(13) . '//methods', $str);
        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateNewByTemplate($formData) {

        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'std_new_by.html.twig';

        //Archivo Destino
        if ($entity1Data['vendorName']) {
            $destFile = $entity1Data['bundleDir'];
        } else {
            $destFile = $this->container->getParameter('kernel.root_dir');
        }
        $destFile .= DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'new_by_' . strtolower($entity2Data['className']) . '.html.twig'
        ;

        //Reemplazo Strings
        $f = fopen($resourceFile, 'r');
        $str = fread($f, filesize($resourceFile));

        $str = str_replace('entity1name', strtolower($entity1Data['className']), $str);
        $str = str_replace('entity2name', strtolower($entity2Data['className']), $str);
        $str = str_replace('entity2Name', lcfirst($entity2Data['className']), $str);
        $str = str_replace('Entity2Name', $entity2Data['className'], $str);

        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateCreateByMethod($formData) {
        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo Destino
        $destFile = $entity1Data['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] . 'AdminController.php';

        //Me fijo si existe el archivo EntityAdminController
        if (!file_exists($destFile)) {
            throw $this->createNotFoundException('No existe el archivo ' . $destFile);
        }
        //Leo el archivo para buscar el string //methods
        try {
            $f = fopen($destFile, 'r');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $str = fread($f, filesize($destFile));
        fclose($f);
        if (FALSE === strrpos($str, '//methods')) {
            throw $this->createNotFoundException('No se encontré el string "//methods" para poder escribir el método. '
                    . 'Agregar //methods donde se va a escribir el método.');
        }

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . '_StdCreateByAction.php';

        //Leo archivo STD
        $f1 = fopen($resourceFile, 'r');
        $str1 = fread($f1, filesize($resourceFile));
        fclose($f1);

        //Reemplazo Strings
        $str1 = str_replace('Vendor1Name', $entity1Data['vendorName'], $str1);
        $str1 = str_replace('Bundle1Name', $entity1Data['bundleName'], $str1);
        $str1 = str_replace('Entity1Name', $entity1Data['className'], $str1);
        $str1 = str_replace('entity1name', strtolower($entity1Data['className']), $str1);
        $str1 = str_replace('Vendor2Name', $entity2Data['vendorName'], $str1);
        $str1 = str_replace('Bundle2Name', $entity2Data['bundleName'], $str1);
        $str1 = str_replace('Entity2Name', $entity2Data['className'], $str1);
        $str1 = str_replace('entity2name', strtolower($entity2Data['className']), $str1);
        $str1 = str_replace('entity2Name', lcfirst($entity2Data['className']), $str1);

        $str = str_replace('//methods', $str1 . chr(13) . chr(13) . '//methods', $str);
        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateShowByMethod($formData) {
        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo Destino
        $destFile = $entity1Data['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] . 'AdminController.php';

        //Me fijo si existe el archivo EntityAdminController
        if (!file_exists($destFile)) {
            throw $this->createNotFoundException('No existe el archivo ' . $destFile);
        }
        //Leo el archivo para buscar el string //methods
        try {
            $f = fopen($destFile, 'r');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $str = fread($f, filesize($destFile));
        fclose($f);
        if (FALSE === strrpos($str, '//methods')) {
            throw $this->createNotFoundException('No se encontré el string "//methods" para poder escribir el método. '
                    . 'Agregar //methods donde se va a escribir el método.');
        }

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . '_StdShowByAction.php';

        //Leo archivo STD
        $f1 = fopen($resourceFile, 'r');
        $str1 = fread($f1, filesize($resourceFile));
        fclose($f1);

        //Reemplazo Strings
        $str1 = str_replace('Vendor1Name', $entity1Data['vendorName'], $str1);
        $str1 = str_replace('Bundle1Name', $entity1Data['bundleName'], $str1);
        $str1 = str_replace('Entity1Name', $entity1Data['className'], $str1);
        $str1 = str_replace('entity1name', strtolower($entity1Data['className']), $str1);
        $str1 = str_replace('Vendor2Name', $entity2Data['vendorName'], $str1);
        $str1 = str_replace('Bundle2Name', $entity2Data['bundleName'], $str1);
        $str1 = str_replace('Entity2Name', $entity2Data['className'], $str1);
        $str1 = str_replace('entity2name', strtolower($entity2Data['className']), $str1);
        $str1 = str_replace('entity2Name', lcfirst($entity2Data['className']), $str1);

        $str = str_replace('//methods', $str1 . chr(13) . chr(13) . '//methods', $str);
        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateShowByTemplate($formData) {

        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'std_show_by.html.twig';

        //Archivo Destino
        if ($entity1Data['vendorName']) {
            $destFile = $entity1Data['bundleDir'];
        } else {
            $destFile = $this->container->getParameter('kernel.root_dir');
        }
        $destFile .= DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'show_by_' . strtolower($entity2Data['className']) . '.html.twig'
        ;

        //Reemplazo Strings
        $f = fopen($resourceFile, 'r');
        $str = fread($f, filesize($resourceFile));

        $str = str_replace('Vendor1Name', $entity1Data['vendorName'], $str);
        $str = str_replace('Bundle1Name', $entity1Data['bundleName'], $str);
        $str = str_replace('entity1name', strtolower($entity1Data['className']), $str);
        $str = str_replace('entity2name', strtolower($entity2Data['className']), $str);
        $str = str_replace('entity2Name', lcfirst($entity2Data['className']), $str);
        $str = str_replace('Entity1Name', $entity1Data['className'], $str);
        $str = str_replace('Entity2Name', $entity2Data['className'], $str);

        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateEditByMethod($formData) {
        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo Destino
        $destFile = $entity1Data['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] . 'AdminController.php';

        //Me fijo si existe el archivo EntityAdminController
        if (!file_exists($destFile)) {
            throw $this->createNotFoundException('No existe el archivo ' . $destFile);
        }
        //Leo el archivo para buscar el string //methods
        try {
            $f = fopen($destFile, 'r');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $str = fread($f, filesize($destFile));
        fclose($f);
        if (FALSE === strrpos($str, '//methods')) {
            throw $this->createNotFoundException('No se encontré el string "//methods" para poder escribir el método. '
                    . 'Agregar //methods donde se va a escribir el método.');
        }

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . '_StdEditByAction.php';

        //Leo archivo STD
        $f1 = fopen($resourceFile, 'r');
        $str1 = fread($f1, filesize($resourceFile));
        fclose($f1);

        //Reemplazo Strings
        $str1 = str_replace('Vendor1Name', $entity1Data['vendorName'], $str1);
        $str1 = str_replace('Bundle1Name', $entity1Data['bundleName'], $str1);
        $str1 = str_replace('Entity1Name', $entity1Data['className'], $str1);
        $str1 = str_replace('entity1name', strtolower($entity1Data['className']), $str1);
        $str1 = str_replace('Vendor2Name', $entity2Data['vendorName'], $str1);
        $str1 = str_replace('Bundle2Name', $entity2Data['bundleName'], $str1);
        $str1 = str_replace('Entity2Name', $entity2Data['className'], $str1);
        $str1 = str_replace('entity2name', strtolower($entity2Data['className']), $str1);
        $str1 = str_replace('entity2Name', lcfirst($entity2Data['className']), $str1);

        $str = str_replace('//methods', $str1 . chr(13) . chr(13) . '//methods', $str);
        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateEditByTemplate($formData) {

        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'std_edit_by.html.twig';

        //Archivo Destino
        if ($entity1Data['vendorName']) {
            $destFile = $entity1Data['bundleDir'];
        } else {
            $destFile = $this->container->getParameter('kernel.root_dir');
        }
        $destFile .= DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'edit_by_' . strtolower($entity2Data['className']) . '.html.twig'
        ;

        //Reemplazo Strings
        $f = fopen($resourceFile, 'r');
        $str = fread($f, filesize($resourceFile));

        $str = str_replace('Vendor1Name', $entity1Data['vendorName'], $str);
        $str = str_replace('Bundle1Name', $entity1Data['bundleName'], $str);
        $str = str_replace('entity1name', strtolower($entity1Data['className']), $str);
        $str = str_replace('entity2name', strtolower($entity2Data['className']), $str);
        $str = str_replace('entity2Name', lcfirst($entity2Data['className']), $str);
        $str = str_replace('Entity1Name', $entity1Data['className'], $str);
        $str = str_replace('Entity2Name', $entity2Data['className'], $str);

        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateUpdateByMethod($formData) {
        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo Destino
        $destFile = $entity1Data['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] . 'AdminController.php';

        //Me fijo si existe el archivo EntityAdminController
        if (!file_exists($destFile)) {
            throw $this->createNotFoundException('No existe el archivo ' . $destFile);
        }
        //Leo el archivo para buscar el string //methods
        try {
            $f = fopen($destFile, 'r');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $str = fread($f, filesize($destFile));
        fclose($f);
        if (FALSE === strrpos($str, '//methods')) {
            throw $this->createNotFoundException('No se encontré el string "//methods" para poder escribir el método. '
                    . 'Agregar //methods donde se va a escribir el método.');
        }

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . '_StdUpdateByAction.php';

        //Leo archivo STD
        $f1 = fopen($resourceFile, 'r');
        $str1 = fread($f1, filesize($resourceFile));
        fclose($f1);

        //Reemplazo Strings
        $str1 = str_replace('Vendor1Name', $entity1Data['vendorName'], $str1);
        $str1 = str_replace('Bundle1Name', $entity1Data['bundleName'], $str1);
        $str1 = str_replace('Entity1Name', $entity1Data['className'], $str1);
        $str1 = str_replace('entity1name', strtolower($entity1Data['className']), $str1);
        $str1 = str_replace('Vendor2Name', $entity2Data['vendorName'], $str1);
        $str1 = str_replace('Bundle2Name', $entity2Data['bundleName'], $str1);
        $str1 = str_replace('Entity2Name', $entity2Data['className'], $str1);
        $str1 = str_replace('entity2name', strtolower($entity2Data['className']), $str1);
        $str1 = str_replace('entity2Name', lcfirst($entity2Data['className']), $str1);

        $str = str_replace('//methods', $str1 . chr(13) . chr(13) . '//methods', $str);
        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateListDeleteByFormMethod($formData) {
        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo Destino
        $destFile = $entity1Data['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] . 'AdminController.php';

        //Me fijo si existe el archivo EntityAdminController
        if (!file_exists($destFile)) {
            throw $this->createNotFoundException('No existe el archivo ' . $destFile);
        }
        //Leo el archivo para buscar el string //methods
        try {
            $f = fopen($destFile, 'r');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $str = fread($f, filesize($destFile));
        fclose($f);
        if (FALSE === strrpos($str, '//methods')) {
            throw $this->createNotFoundException('No se encontré el string "//methods" para poder escribir el método. '
                    . 'Agregar //methods donde se va a escribir el método.');
        }

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . '_StdListDeleteByFormAction.php';

        //Leo archivo STD
        $f1 = fopen($resourceFile, 'r');
        $str1 = fread($f1, filesize($resourceFile));
        fclose($f1);

        //Reemplazo Strings
        $str1 = str_replace('Vendor1Name', $entity1Data['vendorName'], $str1);
        $str1 = str_replace('Bundle1Name', $entity1Data['bundleName'], $str1);
        $str1 = str_replace('Entity1Name', $entity1Data['className'], $str1);
        $str1 = str_replace('entity1name', strtolower($entity1Data['className']), $str1);
        $str1 = str_replace('Vendor2Name', $entity2Data['vendorName'], $str1);
        $str1 = str_replace('Bundle2Name', $entity2Data['bundleName'], $str1);
        $str1 = str_replace('Entity2Name', $entity2Data['className'], $str1);
        $str1 = str_replace('entity2name', strtolower($entity2Data['className']), $str1);
        $str1 = str_replace('entity2Name', lcfirst($entity2Data['className']), $str1);

        $str = str_replace('//methods', $str1 . chr(13) . chr(13) . '//methods', $str);
        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateListDeleteByMethod($formData) {
        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo Destino
        $destFile = $entity1Data['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] . 'AdminController.php';

        //Me fijo si existe el archivo EntityAdminController
        if (!file_exists($destFile)) {
            throw $this->createNotFoundException('No existe el archivo ' . $destFile);
        }
        //Leo el archivo para buscar el string //methods
        try {
            $f = fopen($destFile, 'r');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $str = fread($f, filesize($destFile));
        fclose($f);
        if (FALSE === strrpos($str, '//methods')) {
            throw $this->createNotFoundException('No se encontré el string "//methods" para poder escribir el método. '
                    . 'Agregar //methods donde se va a escribir el método.');
        }

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . '_StdListDeleteByAction.php';

        //Leo archivo STD
        $f1 = fopen($resourceFile, 'r');
        $str1 = fread($f1, filesize($resourceFile));
        fclose($f1);

        //Reemplazo Strings
        $str1 = str_replace('Vendor1Name', $entity1Data['vendorName'], $str1);
        $str1 = str_replace('Bundle1Name', $entity1Data['bundleName'], $str1);
        $str1 = str_replace('Entity1Name', $entity1Data['className'], $str1);
        $str1 = str_replace('entity1name', strtolower($entity1Data['className']), $str1);
        $str1 = str_replace('Vendor2Name', $entity2Data['vendorName'], $str1);
        $str1 = str_replace('Bundle2Name', $entity2Data['bundleName'], $str1);
        $str1 = str_replace('Entity2Name', $entity2Data['className'], $str1);
        $str1 = str_replace('entity2name', strtolower($entity2Data['className']), $str1);
        $str1 = str_replace('entity2Name', lcfirst($entity2Data['className']), $str1);

        $str = str_replace('//methods', $str1 . chr(13) . chr(13) . '//methods', $str);
        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    public function generateBulkByMethod($formData) {
        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo Destino
        $destFile = $entity1Data['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . $entity1Data['className'] . 'AdminController.php';

        //Me fijo si existe el archivo EntityAdminController
        if (!file_exists($destFile)) {
            throw $this->createNotFoundException('No existe el archivo ' . $destFile);
        }
        //Leo el archivo para buscar el string //methods
        try {
            $f = fopen($destFile, 'r');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $str = fread($f, filesize($destFile));
        fclose($f);
        if (FALSE === strrpos($str, '//methods')) {
            throw $this->createNotFoundException('No se encontré el string "//methods" para poder escribir el método. '
                    . 'Agregar //methods donde se va a escribir el método.');
        }

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . '_StdBulkByAction.php';

        //Leo archivo STD
        $f1 = fopen($resourceFile, 'r');
        $str1 = fread($f1, filesize($resourceFile));
        fclose($f1);

        //Reemplazo Strings
        $str1 = str_replace('Vendor1Name', $entity1Data['vendorName'], $str1);
        $str1 = str_replace('Bundle1Name', $entity1Data['bundleName'], $str1);
        $str1 = str_replace('Entity1Name', $entity1Data['className'], $str1);
        $str1 = str_replace('entity1name', strtolower($entity1Data['className']), $str1);
        $str1 = str_replace('Vendor2Name', $entity2Data['vendorName'], $str1);
        $str1 = str_replace('Bundle2Name', $entity2Data['bundleName'], $str1);
        $str1 = str_replace('Entity2Name', $entity2Data['className'], $str1);
        $str1 = str_replace('entity2name', strtolower($entity2Data['className']), $str1);
        $str1 = str_replace('entity2Name', lcfirst($entity2Data['className']), $str1);

        $str = str_replace('//methods', $str1 . chr(13) . chr(13) . '//methods', $str);
        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    private function generateActionByRoute($formData) {

        $entity1Data = $this->getEntityData($formData['entity1ShortcutName']);
        $entity2Data = $this->getEntityData($formData['entity2ShortcutName']);

        //Archivo Destino
        $destFile = $entity1Data['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'config' .
                DIRECTORY_SEPARATOR . 'routing' .
                DIRECTORY_SEPARATOR . strtolower($entity1Data['className'] . '_admin.yml');

        //Me fijo si existe el archivo entity_admin.yml
        if (!file_exists($destFile)) {
            throw $this->createNotFoundException('No existe el archivo ' . $destFile);
        }
        $f = fopen($destFile, 'r');
        $str = fread($f, filesize($destFile));
        fclose($f);

        $route = '';

        //Ruta listby
        if (array_search('gen-listby-route', $formData['tasks']) !== false) {
            $route .= <<<'EOD'
entity1name_listbyentity2name_admin:
    path:  /entity1name/{entity2nameId}/listbyentity2name
    defaults: { _controller: Vendor1Name\Bundle1Name\Admin\Controller\Entity1NameAdminController::listByEntity2NameAction}
EOD;
        }
        //Ruta newby
        if (array_search('gen-newby-route', $formData['tasks']) !== false) {
            $route .= <<<'EOD'
   
   
entity1name_newbyentity2name_admin:
    path:  /entity1name/{entity2nameId}/newbyentity2name
    defaults: { _controller: Vendor1Name\Bundle1Name\Admin\Controller\Entity1NameAdminController::newByEntity2NameAction}
EOD;
        }
        //Ruta createby
        if (array_search('gen-createby-route', $formData['tasks']) !== false) {
            $route .= <<<'EOD'
   
   
entity1name_createbyentity2name_admin:
    path:  /entity1name/createbyentity2name
    defaults: { _controller: Vendor1Name\Bundle1Name\Admin\Controller\Entity1NameAdminController::createByEntity2NameAction}
    requirements: { _method: post|put }
EOD;
        }
        //Ruta editby
        if (array_search('gen-editby-route', $formData['tasks']) !== false) {
            $route .= <<<'EOD'
   
   
entity1name_editbyentity2name_admin:
    path:  /entity1name/{id}/editbyentity2name
    defaults: { _controller: Vendor1Name\Bundle1Name\Admin\Controller\Entity1NameAdminController::editByEntity2NameAction}
EOD;
        }
        //Ruta updateby
        if (array_search('gen-updateby-route', $formData['tasks']) !== false) {
            $route .= <<<'EOD'
   
   
entity1name_updatebyentity2name_admin:
    path:  /entity1name/{id}/updatebyentity2name
    defaults: { _controller: Vendor1Name\Bundle1Name\Admin\Controller\Entity1NameAdminController::updateByEntity2NameAction}
    requirements: { _method: post|put }
EOD;
        }
        //Ruta showby
        if (array_search('gen-showby-route', $formData['tasks']) !== false) {
            $route .= <<<'EOD'
   
   
entity1name_showbyentity2name_admin:
    path:  /entity1name/{id}/showbyentity2name
    defaults: { _controller: Vendor1Name\Bundle1Name\Admin\Controller\Entity1NameAdminController::showByEntity2NameAction}
EOD;
        }
        //Ruta deleteby
        if (array_search('gen-deleteby-route', $formData['tasks']) !== false) {
            $route .= <<<'EOD'
   
   
entity1name_deletebyentity2name_admin:
    path:  /entity1name/{id}/deletebyentity2name
    defaults: { _controller: Vendor1Name\Bundle1Name\Admin\Controller\Entity1NameAdminController::deleteByEntity2NameAction}
    requirements: { _method: post|delete }
EOD;
        }
        //Ruta listdeleteformby 
        if (array_search('gen-listdeleteformby-route', $formData['tasks']) !== false) {
            $route .= <<<'EOD'
   
   
entity1name_listdeleteformbyentity2name_admin:
    path:  /entity1name/{id}/listdeleteformbyentity2name
    defaults: { _controller: Vendor1Name\Bundle1Name\Admin\Controller\Entity1NameAdminController::listDeleteFormByEntity2NameAction}
    requirements: { _method: get }
EOD;
        }
        //Ruta bulkby
        if (array_search('gen-bulkby-route', $formData['tasks']) !== false) {
            $route .= <<<'EOD'
   
   
entity1name_bulkbyentity2name_admin:
    path:  /entity1name/bulkbyentity2name
    defaults: { _controller: Vendor1Name\Bundle1Name\Admin\Controller\Entity1NameAdminController::bulkByEntity2NameAction}
    requirements: { _method: post|put }
EOD;
        }

        if ($route != '') {
            if (FALSE === strrpos($str, "#routes")) {
                throw $this->createNotFoundException('No encontré el string "#routes" para poder escribir la nueva ruta. '
                        . 'Agregar #routes donde se va a escribir la ruta. Archivo: ' . $destFile);
            }
        }

        // Acme/TestBundle:Test
        if ($entity1Data['vendorName']) {
            $route = str_replace('Vendor1Name', $entity1Data['vendorName'], $route);
        }
        // AppBundle:Test
        else {
            $route = str_replace('Vendor1Name\\', '', $route);
        }

        $route = str_replace('Bundle1Name', $entity1Data['bundleName'], $route);
        $route = str_replace('Entity1Name', $entity1Data['className'], $route);
        $route = str_replace('entity1name', strtolower($entity1Data['className']), $route);
        $route = str_replace('entity2name', strtolower($entity2Data['className']), $route);
        $route = str_replace('Entity2Name', $entity2Data['className'], $route);

        $str = str_replace('#routes', $route . chr(13) . chr(13) . '#routes', $str);

        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

}
