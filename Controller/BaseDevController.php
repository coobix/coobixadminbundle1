<?php

namespace Coobix\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Base Dev controller.
 */
class BaseDevController extends Controller
{

    protected $stdFileAdminViewsPath = '';

    public function __construct() {
        $this->stdFilesAdminViewsPath = join(DIRECTORY_SEPARATOR, array('..', 'vendor', 'coobix', 'admin-bundle', 'Coobix', 'AdminBundle', 'Resources', 'stdFiles', 'stdBundle', 'Resources', 'views', 'Admin'))
        ;
        $this->stdBundleFiles = join(DIRECTORY_SEPARATOR, array('..', 'vendor', 'coobix', 'admin-bundle', 'Coobix', 'AdminBundle', 'Resources', 'stdFiles', 'stdBundle'))
        ;
    }

    public function indexAction() {
        return $this->render('CoobixAdminBundle:Dev:home.html.twig', array(
        ));
    }

    public function todoAction() {
        return $this->render('CoobixAdminBundle:Dev:todo.html.twig', array(
        ));
    }

    /**
     * Creates a form to create a CRUD entity.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createAdminCrudCreateForm($defaultData = array()) {

        $form = $this->createFormBuilder($defaultData)
                ->setAction($this->generateUrl('dev_crudcreate'))
                ->setMethod('GET')
                ->add('entityShortcutName', TextType::class, array('label' => 'Entity Shortcut Name (Acme/TestBundle:Test)'))
                ->add('tasks', ChoiceType::class, array(
                    'choices' => array(
                        'Generar la estructura de directorios' => 'gen-dir',
                        'Generar entidad Admin' => 'gen-entity',
                        'Generar controlador de entidad Admin' => 'gen-controller',
                        'Generar formulado del listado' => 'gen-list-search-form',
                        'Generar template del listado' => 'gen-list',
                        'Generar template del show' => 'gen-show',
                        'Generar routing' => 'gen-routing',
                    ),
                    'expanded' => true,
                    'multiple' => true,
                    'attr' => array('class' => 'form-group-checkbox')
                ))
                ->add('submit', SubmitType::class, array('label' => 'Crear'))
                ->getForm();
        return $form;
    }

    /**
     * Display a form to do Admin CRUD's tasks.
     */
    public function adminCrudNewAction() {

        $form = $this->createAdminCrudCreateForm();
        return $this->render('CoobixAdminBundle:Dev:newCrud.html.twig', array(
                    'form' => $form->createView(),
                    'serviceConfig' => '',
                    'tasks' => array(),
        ));
    }

    /**
     * Execute selected Admin CRUD's tasks.
     */
    public function adminCrudCreateAction(Request $request) {

        $form = $this->createAdminCrudCreateForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $formData = $form->getData();

            $tasks = array();
            if (array_search('gen-dir', $formData['tasks']) !== false) {
                $tasks[] = $this->generateDirectoryStructrure($formData['entityShortcutName']);
            }
            if (array_search('gen-entity', $formData['tasks']) !== false) {
                $tasks[] = $this->generateAdminEntity($formData['entityShortcutName']);
            }
            if (array_search('gen-controller', $formData['tasks']) !== false) {
                $tasks[] = $this->generateAdminController($formData['entityShortcutName']);
            }
            if (array_search('gen-list-search-form', $formData['tasks']) !== false) {
                $tasks[] = $this->generateAdminListSearchForm($formData['entityShortcutName']);
            }
            if (array_search('gen-list', $formData['tasks']) !== false) {
                $tasks[] = $this->generateList($formData['entityShortcutName']);
            }
            if (array_search('gen-show', $formData['tasks']) !== false) {
                $tasks[] = $this->generateShow($formData['entityShortcutName']);
            }
            if (array_search('gen-routing', $formData['tasks']) !== false) {
                $tasks[] = $this->generateRouting($formData['entityShortcutName']);
            }

            $entityData = $this->getEntityData($formData['entityShortcutName']);

            $serviceConfig = ''
                    . '<pre>'
                    . '//config.yml o src/' . ($entityData['vendorName'] ? $entityData['vendorName'] . '/' : '') . $entityData['bundleName'] . '/Resources/config/services.yml<br>'
                    . 'services:<br>'
                    . '    coobix.admin.' . strtolower($entityData['className']) . ':<br>'
                    . '        class: ' . ($entityData['vendorName'] ? $entityData['vendorName'] . '\\' : '') . $entityData['bundleName'] . '\Admin\Entity\\' . $entityData['className'] . 'Admin<br>'
                    . '        arguments:<br>'
                    . '            - ' . ($entityData['vendorName'] ? $entityData['vendorName'] . '\\' : '') . $entityData['bundleName'] . '\Entity\\' . $entityData['className'] . '<br>'
                    . '</pre>';

            //Config del routing
            $serviceConfig .= ''
                    . '<pre>'
                    . '//src/' . $entityData['className'] . '/' . $entityData['bundleName'] . '/Resources/config/routing.yml<br>'
                    . $entityData['vendorName'] . $entityData['bundleName'] . '_' . strtolower($entityData['className']) . '_admin:<br>'
                    . '    resource: "@' . $entityData['vendorName'] . $entityData['bundleName'] . '/Resources/config/routing/' . strtolower($entityData['className']) . '_admin.yml"<br>'
                    . '    prefix:   /admin<br>'
                    . '</pre>';
        } else {
            echo "error";
            $tasks = array();
            $serviceConfig = '';
        }

        return $this->render('CoobixAdminBundle:Dev:newCrud.html.twig', array(
                    'form' => $form->createView(),
                    'serviceConfig' => $serviceConfig,
                    'tasks' => $tasks,
        ));
    }

    public function getEntityData($entityShortcutNameParam) {

        // Acme/TestBundle:Test
        $entityShortcutName = explode(':', $entityShortcutNameParam);
        $aux = explode('/', $entityShortcutName[0]);

        // AppBundle:Test
        if (count($aux) == 1) {
            $vendorName = null;
            $bundleName = $aux[0];
            $bundleDir = $this->container->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR
                    . '..' . DIRECTORY_SEPARATOR . 'src' .
                    DIRECTORY_SEPARATOR . $bundleName;
        }

        // Acme/TestBundle:Test
        else {
            $vendorName = $aux[0];
            $bundleName = $aux[1];
            $bundleDir = $this->container->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR
                    . '..' . DIRECTORY_SEPARATOR . 'src' .
                    DIRECTORY_SEPARATOR . $vendorName . DIRECTORY_SEPARATOR . $bundleName;
        }
        $className = $entityShortcutName[1];

        return array(
            'vendorName' => $vendorName,
            'bundleName' => $bundleName,
            'className' => $className,
            'bundleDir' => $bundleDir,
        );
    }

    private function generateDirectoryStructrure($entityShortcutNameParam) {

        $entityData = $this->getEntityData($entityShortcutNameParam);

        /*
         * 1. Me fijo si existe el directorio del bundle:
         * El directorio puede ser:
         * src/Acme/TestBundle
         * src/AppBundle
         */
        if (!file_exists($entityData['bundleDir'])) {
            throw $this->createNotFoundException('No existe ' . $entityData['bundleDir']);
        }

        /*
         * 2. Me fijo si se puede escribir en directorio del bundle
         */
        if (!is_writable($entityData['bundleDir'])) {
            throw $this->createNotFoundException('No se pudo escribir en ' . $entityData['bundleDir']);
        }

        /*
         * PASO 1 y 2 es solo verificación por que estas carpetas se generan
         * al generar el bundle.
         */

        /*
         * Creo el directorio Admin
         * ------------------------
         * 
         * El directorio puede ser:
         * src/Acme/TestBundle/Admin
         * src/AppBundle/Admin
         */

        $dir = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Admin';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777)) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $dir);
            }
        }

        /*
         * Creo el directorio Admin/Controller
         * -----------------------------------
         * 
         * El directorio puede ser:
         * src/Acme/TestBundle/Admin/Controller
         * src/AppBundle/Admin/Controller
         */
        $dir = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Admin' . DIRECTORY_SEPARATOR . 'Controller';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777)) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $dir);
            }
        }

        /*
         * Creo el directorio Admin/Entity
         * -------------------------------
         * 
         * El directorio puede ser:
         * src/Acme/TestBundle/Admin/Entity
         * src/AppBundle/Admin/Entity
         */
        $dir = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Admin' . DIRECTORY_SEPARATOR . 'Entity';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777)) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $dir);
            }
        }

        /*
         * Creo el directorio Admin/Form
         * -----------------------------
         * 
         * El directorio puede ser:
         * src/Acme/TestBundle/Admin/Form
         * src/AppBundle/Admin/Form
         */
        $dir = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Admin' . DIRECTORY_SEPARATOR . 'Form';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777)) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $dir);
            }
        }

        /*
         * Creo el directorio views/Test/ y views/Test/Admin
         * -------------------------------------------------
         * 
         * El directorio puede ser:
         * src/Acme/TestBundle/Resources/views/Test/Admin
         * app/Resources/views/Test/Admin
         */
        if ($entityData['vendorName']) {
            $dir = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views';
        } else {
            $dir = $this->container->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views';
        }

        // views/Test/
        $dir .= DIRECTORY_SEPARATOR . $entityData['className'];

        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777)) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $dir);
            }
        }

        // views/Test/Admin
        $dir .= DIRECTORY_SEPARATOR . 'Admin';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777)) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $dir);
            }
        }

        /*
         * Creo el directorio Resources
         * -----------------------------
         * 
         * El directorio puede ser:
         * src/Acme/Resources
         * src/AppBundle/Resources
         */
        $dir = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Resources';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777)) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $dir);
            }
        }

        /*
         * Creo el directorio Resources/config
         * -----------------------------
         * 
         * El directorio puede ser:
         * src/Acme/Resources/config
         * src/AppBundle/Resources/config
         */
        $dir = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'config';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777)) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $dir);
            }
        }

        /*
         * Creo el directorio Resources/config
         * -----------------------------
         * 
         * El directorio puede ser:
         * src/Acme/Resources/config
         * src/AppBundle/Resources/config
         */
        $dir = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'config';
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777)) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $dir);
            }
        }

        /*
         * Creo el archivo Resources/config/routing.yml
         * -----------------------------
         * 
         * El directorio puede ser:
         * src/Acme/Resources/config/routing.yml
         * src/AppBundle/Resources/config/routing.yml
         */
        $file = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'routing.yml';
        if (!file_exists($file)) {

            if (!($fp = fopen($file, "a"))) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $file);
            }
            fclose($fp);
        }

        /*
         * Creo el archivo Resources/config/services.yml
         * -----------------------------
         * 
         * El directorio puede ser:
         * src/Acme/Resources/config/services.yml
         * src/AppBundle/Resources/config/services.yml
         */
        $file = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'services.yml';
        if (!file_exists($file)) {

            if (!($fp = fopen($file, "a"))) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $file);
            }
            fwrite($fp, 'services:');
            fclose($fp);
        }

        /*
         * Creo el directorio Resources/config/routing
         * -------------------------------------------
         * 
         * El directorio puede ser:
         * src/Acme/Resources/config/services.yml
         * src/AppBundle/Resources/config/services.yml
         */
        $dir = $entityData['bundleDir'] . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'routing';
        if (!file_exists($dir)) {

            if (!mkdir($dir, 0777)) {
                throw $this->createNotFoundException('No se pudo crear el directorio ' . $dir);
            }
        }
    }

    private function generateRouting($entityShortcutNameParam) {

        $entityData = $this->getEntityData($entityShortcutNameParam);

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'config' .
                DIRECTORY_SEPARATOR . 'routing' .
                DIRECTORY_SEPARATOR . 'stdentity_admin.yml';

        //Archivo Destino
        $destFile = $entityData['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'config' .
                DIRECTORY_SEPARATOR . 'routing' .
                DIRECTORY_SEPARATOR . strtolower($entityData['className'] . '_admin.yml');

        //Reemplazo Strings
        $f = fopen($resourceFile, 'r');
        $str = fread($f, filesize($resourceFile));
        fclose($f);

        // Acme/TestBundle:Test
        if ($entityData['vendorName']) {
            $str = str_replace('VendorName', $entityData['vendorName'], $str);
        }
        // AppBundle:Test
        else {
            $str = str_replace('VendorName\\', '', $str);
        }

        $str = str_replace('BundleName', $entityData['bundleName'], $str);
        $str = str_replace('EntityName', $entityData['className'], $str);
        $str = str_replace('entityname', strtolower($entityData['className']), $str);

        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    private function generateAdminEntity($entityShortcutNameParam) {

        $entityData = $this->getEntityData($entityShortcutNameParam);

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Entity' .
                DIRECTORY_SEPARATOR . 'StdEntityAdmin.php';

        //Archivo Destino
        $destFile = $entityData['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Entity' .
                DIRECTORY_SEPARATOR . $entityData['className'] . 'Admin.php';

        //Reemplazo Strings
        $f = fopen($resourceFile, 'r');
        $str = fread($f, filesize($resourceFile));
        fclose($f);

        // Acme/TestBundle:Test
        if ($entityData['vendorName']) {
            $str = str_replace('VendorName', $entityData['vendorName'], $str);
        }
        // Acme/TestBundle:Test
        else {
            $str = str_replace('VendorName\\', '', $str);
        }

        $str = str_replace('BundleName', $entityData['bundleName'], $str);
        $str = str_replace('EntityName', $entityData['className'], $str);

        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);

        return true;
    }

    private function generateAdminController($entityShortcutNameParam) {

        $entityData = $this->getEntityData($entityShortcutNameParam);

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . 'StdAdminController.php';

        //Archivo Destino
        $destFile = $entityData['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Controller' .
                DIRECTORY_SEPARATOR . $entityData['className'] . 'AdminController.php';

        //Reemplazo Strings
        $f = fopen($resourceFile, 'r');
        $str = fread($f, filesize($resourceFile));
        fclose($f);

        // Acme/TestBundle:Test
        if ($entityData['vendorName']) {
            $str = str_replace('VendorName', $entityData['vendorName'], $str);
        }
        // Acme/TestBundle:Test
        else {
            $str = str_replace('VendorName\\', '', $str);
        }

        $str = str_replace('BundleName', $entityData['bundleName'], $str);
        $str = str_replace('EntityName', $entityData['className'], $str);

        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    private function generateShow($entityShortcutNameParam) {

        $entityData = $this->getEntityData($entityShortcutNameParam);

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'StdShow.html.twig';

        //Archivo Destino
        if ($entityData['vendorName']) {
            $destFile = $entityData['bundleDir'];
        } else {
            $destFile = $this->container->getParameter('kernel.root_dir');
        }
        $destFile .= DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . $entityData['className'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'show.html.twig'
        ;

        //Reemplazo Strings
        $f = fopen($resourceFile, 'r');
        $str = fread($f, filesize($resourceFile));
        fclose($f);

        $str = str_replace('EntityName', $entityData['className'], $str);

        // Acme/TestBundle:Test
        if ($entityData['vendorName']) {
            $classNameSpace = "\\" . $entityData['vendorName'] . "\\" . $entityData['bundleName'] . "\\Entity\\" . $entityData['className'];
        }
        // Acme/TestBundle:Test
        else {
            $classNameSpace = "\\" . $entityData['bundleName'] . "\\Entity\\" . $entityData['className'];
        }

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        $metaData = $em->getClassMetadata($classNameSpace);

        $fieldNames = $metaData->getFieldNames();

        //Reemplazo la parte de los valores
        foreach ($fieldNames as $f) {
            $field = $metaData->getFieldMapping($f);

            switch ($field['type']) {
                case 'datetime':
                    $str = str_replace('{# show-field #}', ''
                            . '<div class="row border-bottom padding-vert-10">' . chr(13) . chr(9)
                            . '<div class="col-md-2 ">' . ucfirst($field['fieldName']) . ':</div>' . chr(13) . chr(9)
                            . '<div class="col-md-10 ">{{ entity.get' . ucfirst($field['fieldName']) . '() | date("d/m/Y H:i:s") }}</div> ' . chr(13) . chr(9)
                            . '</div>' . chr(13) . chr(9)
                            . chr(13) . chr(9) . chr(9) . '{# show-field #}', $str);
                    break;
                case 'date':
                    $str = str_replace('{# show-field #}', ''
                            . '<div class="row border-bottom padding-vert-10">' . chr(13) . chr(9)
                            . '<div class="col-md-2 ">' . ucfirst($field['fieldName']) . ':</div>' . chr(13) . chr(9)
                            . '<div class="col-md-10 ">{{ entity.get' . ucfirst($field['fieldName']) . '() | date("d/m/Y") }}</div> ' . chr(13) . chr(9)
                            . '</div>' . chr(13) . chr(9)
                            . chr(13) . chr(9) . chr(9) . '{# show-field #}', $str);
                default :
                    $str = str_replace('{# show-field #}', ''
                            . '<div class="row border-bottom padding-vert-10">' . chr(13) . chr(9)
                            . '<div class="col-md-2 ">' . ucfirst($field['fieldName']) . ':</div>' . chr(13) . chr(9)
                            . '<div class="col-md-10 ">{{ entity.get' . ucfirst($field['fieldName']) . '() }}</div> ' . chr(13) . chr(9)
                            . '</div>' . chr(13) . chr(9)
                            . chr(13) . chr(9) . chr(9) . '{# show-field #}', $str);
                    break;
            }
        }

        $associationMappings = $metaData->getAssociationMappings();
        foreach ($associationMappings as $field) {
            $str = str_replace('{# show-field #}', ''
                    . '<div class="row border-bottom padding-vert-10">' . chr(13) . chr(9)
                    . '<div class="col-md-2 ">' . ucfirst($field['fieldName']) . ':</div>'
                    . '<div class="col-md-10 ">{{ entity.get' . ucfirst($field['fieldName']) . '() }}</div> '
                    . '</div>' . chr(13) . chr(9)
                    . chr(13) . chr(9) . chr(9) . '{# show-field #}', $str);
            break;
        }

        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

    private function generateList($entityShortcutNameParam) {

        $entityData = $this->getEntityData($entityShortcutNameParam);

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'StdList.html.twig';

        //Archivo Destino
        if ($entityData['vendorName']) {
            $destFile = $entityData['bundleDir'];
        } else {
            $destFile = $this->container->getParameter('kernel.root_dir');
        }
        $destFile .= DIRECTORY_SEPARATOR . 'Resources' .
                DIRECTORY_SEPARATOR . 'views' .
                DIRECTORY_SEPARATOR . $entityData['className'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'list.html.twig'
        ;

        //Reemplazo Strings
        $f = fopen($resourceFile, 'r');
        $str = fread($f, filesize($resourceFile));
        fclose($f);

        // Acme/TestBundle:Test
        if ($entityData['vendorName']) {
            $classNameSpace = "\\" . $entityData['vendorName'] . "\\" . $entityData['bundleName'] . "\\Entity\\" . $entityData['className'];
        }
        // Acme/TestBundle:Test
        else {
            $classNameSpace = "\\" . $entityData['bundleName'] . "\\Entity\\" . $entityData['className'];
        }

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        $metaData = $em->getClassMetadata($classNameSpace);

        $fieldNames = $metaData->getFieldNames();
        foreach ($fieldNames as $f) {

            $field = $metaData->getFieldMapping($f);
            if ($field['fieldName'] == 'id') {
                continue;
            }


            /*
              Array (
              [fieldName] => tipoResolucion
              [type] => integer
              [scale] => 0
              [length] =>
              [unique] =>
              [nullable] =>
              [precision] => 0
              [columnName] => tipoResolucion
              )
             */


            switch ($field['type']) {
                default :
                    $str = str_replace('{# Col-title #}', ''
                            . '<th><a href="{{ list.getColFilterUrl(\'' . $field['fieldName'] . '\') }}">' . ucfirst($field['fieldName']) . '</a></th>'
                            . chr(13) . chr(9) . chr(9) . '{# Col-title #}', $str);
                    break;
            }
        }

        $associationMappings = $metaData->getAssociationMappings();
        foreach ($associationMappings as $field) {
            $str = str_replace('{# Col-title #}', ''
                    . '<th><a href="{{ list.getColFilterUrl(\'' . $field['fieldName'] . '\') }}">' . ucfirst($field['fieldName']) . '</a></th>'
                    . chr(13) . chr(9) . chr(9) . '{# Col-title #}', $str);
            break;
        }

        //Reemplazo la parte de los valores
        foreach ($fieldNames as $f) {
            $field = $metaData->getFieldMapping($f);

            if ($field['fieldName'] == 'id') {
                continue;
            }

            switch ($field['type']) {
                case 'datetime':
                    $str = str_replace('{# Col-value #}', ''
                            . '<td><a href="{{ path(\'admin_show\', routeParams) }}">'
                            . '{{ e.get' . ucfirst($field['fieldName']) . '() | date("d/m/Y H:i:s") }}</a></td>'
                            . chr(13) . chr(9) . chr(9) . '{# Col-value #}', $str);
                    break;
                case 'date':
                    $str = str_replace('{# Col-value #}', ''
                            . '<td><a href="{{ path(\'admin_show\', routeParams) }}">'
                            . '{{ e.get' . ucfirst($field['fieldName']) . '() | date("d/m/Y") }}</a></td>'
                            . chr(13) . chr(9) . chr(9) . '{# Col-value #}', $str);
                    break;
                default :
                    $str = str_replace('{# Col-value #}', ''
                            . '<td><a href="{{ path(\'admin_show\', routeParams) }}">'
                            . '{{ e.get' . ucfirst($field['fieldName']) . '() }}</a></td>'
                            . chr(13) . chr(9) . chr(9) . '{# Col-value #}', $str);
                    break;
            }
        }

        $associationMappings = $metaData->getAssociationMappings();
        foreach ($associationMappings as $field) {
            $str = str_replace('{# Col-value #}', ''
                    . '<td><a href="{{ path(\'admin_show\', routeParams) }}">'
                    . '{{ e.get' . ucfirst($field['fieldName']) . '() }}</a></td>'
                    . chr(13) . chr(9) . chr(9) . '{# Col-value #}', $str);
            break;
        }

        //El numero de columnas de la tabla
        $numCols = count($fieldNames) + count($associationMappings);
        $str = str_replace('{# num-cols #}', $numCols, $str);

        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
//FIN CREO EL list.html.twig
    }

    private function generateAdminListSearchForm($entityShortcutNameParam) {

        $entityData = $this->getEntityData($entityShortcutNameParam);

        //Archivo original
        $resourceFile = $this->stdBundleFiles .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Form' .
                DIRECTORY_SEPARATOR . 'EntidadListSearchType.php';


        //Archivo Destino
        $destFile = $entityData['bundleDir'] .
                DIRECTORY_SEPARATOR . 'Admin' .
                DIRECTORY_SEPARATOR . 'Form' .
                DIRECTORY_SEPARATOR . $entityData['className'] . 'ListSearchType.php';

        //Reemplazo Strings
        $f = fopen($resourceFile, 'r');
        $str = fread($f, filesize($resourceFile));
        fclose($f);

        // Acme/TestBundle:Test o Acme/TestBundle:Test
        $str = str_replace('VendorName\\', ($entityData['vendorName'] ? $entityData['vendorName'] . "\\" : ''), $str);

        $str = str_replace('BundleName', $entityData['bundleName'], $str);
        $str = str_replace('EntityName', $entityData['className'], $str);
        $str = str_replace('entityname', strtolower($entityData['className']), $str);

        // Acme/TestBundle:Test o Acme/TestBundle:Test
        $classNameSpace = ($entityData['vendorName'] ? "\\" . $entityData['vendorName'] : '') . "\\" . $entityData['bundleName'] . "\\Entity\\" . $entityData['className'];

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        $metaData = $em->getClassMetadata($classNameSpace);
        /*
          tipos de campos
          Type\BaseType
          Type\BirthdayType
          Type\ButtonType
          Type\CheckboxType
          Type\ChoiceType
          Type\CollectionType
          Type\CountryType
          Type\CurrencyType
          Type\DateTimeType
          Type\DateType
          Type\EmailType
          Type\FileType
          Type\FormType
          Type\HiddenType
          Type\IntegerType
          Type\LanguageType
          Type\LocaleType
          Type\MoneyType
          Type\NumberType
          Type\PasswordType
          Type\PercentType
          Type\RadioType
          Type\RangeType
          Type\RepeatedType
          Type\ResetType
          Type\SearchType
          Type\SubmitType
          Type\TextType
          Type\TextareaType
          Type\TimeType
          Type\TimezoneType
          Type\UrlType
         */




        $fieldNames = $metaData->getFieldNames();
        foreach ($fieldNames as $f) {
            $field = $metaData->getFieldMapping($f);

            if ($field['fieldName'] == 'id') {
                continue;
            }
            /*
              Array (
              [fieldName] => tipoResolucion
              [type] => integer
              [scale] => 0
              [length] =>
              [unique] =>
              [nullable] =>
              [precision] => 0
              [columnName] => tipoResolucion
              )
             */


            switch ($field['type']) {
                case 'string':
                case 'text':
                    $str = str_replace('//fields', '->add(\'' . $f . '\', '
                            //. '\'text\', '
                            . 'Type\TextType::class, '
                            . ' array(' . chr(13)
                            . '\'required\' => false,' . chr(13)
                            . '\'label\' => \'' . ucfirst($field['fieldName']) . '\',' . chr(13)
                            . ')) ' . chr(13) . chr(9) . chr(9) . '//fields', $str);
                    break;
                case 'integer':
                    $str = str_replace('//fields', '->add(\'' . $f . '\', '
                            . 'Type\IntegerType::class, '
                            . ' array(' . chr(13)
                            . '\'required\' => false,' . chr(13)
                            . '\'label\' => \'' . ucfirst($field['fieldName']) . '\',' . chr(13)
                            . ')) ' . chr(13) . chr(9) . chr(9) . '//fields', $str);
                    break;
                case 'datetime':
                case 'date':
                    $str = str_replace('//fields', '->add(\'' . $f . '\', '
                            . 'Type\DateType::class, '
                            . ' array(' . chr(13)
                            . '\'required\' => false,' . chr(13)
                            . '\'label\' => \'' . ucfirst($field['fieldName']) . '\',' . chr(13)
                            . ')) ' . chr(13) . chr(9) . chr(9) . '//fields', $str);
                    break;
                default :

                    $str = str_replace('//fields', '->add(\'' . $f . '\', '
                            . 'Type\\' . ucfirst($field['type']) . 'Type::class, '
                            . ' array(' . chr(13)
                            . '\'required\' => false,' . chr(13)
                            . '\'label\' => \'' . ucfirst($field['fieldName']) . '\',' . chr(13)
                            . ')) ' . chr(13) . chr(9) . chr(9) . '//fields', $str);
            }
        }

        $associationMappings = $metaData->getAssociationMappings();
        foreach ($associationMappings as $field) {
            $str = str_replace('//fields', ''
                    . '->add(\'' . $field['fieldName'] . '\', ' . '\'EntityType::class\', ' . chr(13)
                    . ' array(' . chr(13)
                    . '\'class\' => \'' . $field['targetEntity'] . '\' ,' . chr(13)
                    . '\'required\' => false,' . chr(13)
                    . '\'label\' => \'' . ucfirst($field['fieldName']) . '\',' . chr(13)
                    . ')) ' . chr(13) . chr(9) . chr(9) . '//fields', $str);

            /*
              $str = str_replace('//fields', '->add(\'' . $a . '\', '
              . ' null, '
              . ' array(\'required\' => false,)) ' . chr(13) . chr(10) . '//fields', $str);

             */
        }

        $f = fopen($destFile, 'w');
        fwrite($f, $str);
        fclose($f);
    }

}
