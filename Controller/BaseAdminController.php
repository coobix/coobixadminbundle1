<?php

namespace Coobix\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Coobix\AdminBundle\Entity\BaseList;

/**
 * Admin controller.
 */
class BaseAdminController extends Controller
{
    
    /**
     * Show Dashboard.
     */
    public function indexAction() {

        return $this->render('CoobixAdminBundle:Admin:dashboard.html.twig', array(
        ));
    }

    public function editAction($class, $id) {
        $admin = $this->get('coobix.admin.' . $class);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }

        $form = $this->createEditForm($admin, $entity);

        $formOptions = array(
            'id' => $id,
            'label' => 'Borrar'
        );
        $deleteForm = $this->createDeleteForm($admin, $formOptions);

        $template = ($admin->getEditTemplate() != null) ? $admin->getEditTemplate() : 'CoobixAdminBundle:Admin:edit.html.twig';
        return $this->render($template, array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Page class.
     *
     */
    public function updateAction(Request $request, $class, $id) {
        $admin = $this->get('coobix.admin.' . $class);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }

        $form = $this->createEditForm($admin, $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
            //LOG
            $this->get('coobix.log')->create('UPDATE ' . $admin->getClassName() . ': ' . $entity . '. ID: ' . $entity->getId());
            $this->get('session')->getFlashBag()->add('success', 'REGISTRO MODIFICADO.');

            if ($form->get('save')->isClicked()) {
                return $this->redirect($this->generateUrl('admin_show', array('class' => strtolower($admin->getClassName()), 'id' => $id)));
            }

            return $this->redirect($this->generateUrl('admin_edit', array('class' => strtolower($admin->getClassName()), 'id' => $id)));
        }

        $formOptions = array(
            'id' => $id,
            'label' => 'Borrar'
        );
        $deleteForm = $this->createDeleteForm($admin, $formOptions);

        $this->get('session')->getFlashBag()->add('danger', 'EL REGISTRO NO SE PUDO MODIFICAR VERIFIQUE EL FORMULARIO.');

        $template = ($admin->getEditTemplate() != null) ? $admin->getEditTemplate() : 'CoobixAdminBundle:Admin:edit.html.twig';
        return $this->render($template, array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to create a new class.
     *
     */
    public function newAction(Request $request, $class) {
        $admin = $this->get('coobix.admin.' . $class);

        $c = $admin->getClass();
        $entity = new $c;

        $form = $this->createCreateForm($admin, $entity);

        $template = ($admin->getNewTemplate() != null) ? $admin->getNewTemplate() : 'CoobixAdminBundle:Admin:new.html.twig';

        return $this->render($template, array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    public function createAction(Request $request, $class) {
        $admin = $this->get('coobix.admin.' . $class);

        $c = $admin->getClass();
        $entity = new $c;

        $form = $this->createCreateForm($admin, $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            //LOG
            $this->get('session')->getFlashBag()->add('success', 'REGISTRO CREADO.');
            $this->get('coobix.log')->create('CREATE ' . $admin->getClassName() . ': ' . $entity . '. ID: ' . $entity->getId());
            return $this->redirect($this->generateUrl('admin_show', array('class' => strtolower($admin->getClassName()), 'id' => $entity->getId())));
        }

        $this->get('session')->getFlashBag()->add('danger', 'EL REGISTRO NO SE PUDO CREAR VERIFIQUE EL FORMULARIO.');

        $template = ($admin->getNewTemplate() != null) ? $admin->getNewTemplate() : 'CoobixAdminBundle:Admin:new.html.twig';
        return $this->render($template, array(
                    'admin' => $admin,
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    public function notFoundRedirectAction($url, $msg = null) {
        $this->get('session')->getFlashBag()->add('danger', 'NO SE PUDO ENCONTRAR EL REGISTRO.');
        return $this->redirect($url);
    }


    /**
     * Creates a form to create a Page entity.
     *
     * @param Page $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createSearchListForm($admin) {
        $form = $this->createForm($admin->getListSearchForm(), null, array(
            'action' => $this->generateUrl("admin_list", array(
                'class' => strtolower($admin->getClassName()))),
            'method' => 'GET',
        ));

        return $form;
    }


    /**
     * Deletes a class.
     *
     */
    public function deleteAction(Request $request, $id, $class) {
        $admin = $this->get("coobix.admin." . $class);

        $returnUrl = $this->generateUrl("admin_list", array(
            'class' => strtolower($admin->getClassName()),
        ));

        $formOptions = array('id' => $id);
        $form = $this->createDeleteForm($admin, $formOptions);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);

            if (!$entity) {
                $rUrl = $this->generateUrl("admin_list", array('class' => strtolower($admin->getClassName())));
                return $this->forward('CoobixAdminBundle:Admin:notFoundRedirect', array(
                            'url' => $rUrl,
                ));
            }


            try {
                $em->remove($entity);

                $msg = 'DELETE ' . $admin->getClassName() . ': ' . $entity . '. ID: ' . $entity->getId();
                $em->flush();

                //LOG
                $this->get('coobix.log')->create($msg);

                //MSJ FLASH
                $this->get('session')->getFlashBag()->add('success', 'REGISTRO ELIMINADO.');
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('danger', 'EL REGISTRO NO SE PUDO ELIMINAR DEBIDO A QUE TIENE DATOS ASOCIADOS.');
            }
        }

        return $this->redirect($returnUrl);
    }

    public function listDeleteFormAction($class, $id) {
        $admin = $this->get("coobix.admin." . $class);

        $formOptions = array('id' => $id);
        $deleteForm = $this->createDeleteForm($admin, $formOptions);

        return $this->render('CoobixAdminBundle:Admin:list_delete_form.html.twig', array(
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    public function bulkAction(Request $request, $class) {
        $admin = $this->get('coobix.admin.' . $class);

        $ids = explode(',', $request->request->get('bulkIds'));
        unset($ids[0]);

        $action = $request->request->get('action');

        switch ($action) {
            case 'delete':
                $em = $this->getDoctrine()->getManager();

                $q = $em->createQuery(' SELECT e FROM ' . $admin->getEntityShortcutName() . ' e '
                        . ' WHERE e.id IN (:e_ids) ');
                $q->setParameter('e_ids', $ids);
                $entities = $q->getResult();

                $result = $this->deleteBulk($admin, $entities);

                if ($result['success'] > 0) {
                    $this->get('session')->getFlashBag()->add('success', 'SE ELIMINARON ' . $result['success'] . ' REGISTROS.');
                }
                if ($result['error'] > 0) {
                    $this->get('session')->getFlashBag()->add('danger', 'NO SE PUDIERON ELIMINAR ' . $result['error'] . ' REGISTROS.');
                }

                break;
            default :
        }

        return $this->redirect($this->generateUrl("admin_list", array(
                            'class' => strtolower($admin->getClassName()),
        )));
    }


    protected function deleteBulk($admin, $entities) {
        $em = $this->getDoctrine()->getManager();
        $result = array('success' => 0, 'error' => 0);
        foreach ($entities as $e) {
            try {
                $msg = 'BULK DELETE ' . $admin->getClassName() . ': ' . $e . '. ID: ' . $e->getId();
                $em->remove($e);
                $em->flush();
                $this->get('coobix.log')->create($msg);

                $result['success'] += 1;
            } catch (\Exception $e) {
                $result['error'] += 1;
            }
        }

        return $result;
    }

}