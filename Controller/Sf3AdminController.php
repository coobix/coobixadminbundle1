<?php

namespace Coobix\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Coobix\AdminBundle\Controller\BaseAdminController;
use Coobix\AdminBundle\Entity\BaseList;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Admin controller.
 */
class Sf3AdminController extends BaseAdminController
{
    
    /**
     * Finds and displays a Admin class.
     *
     */
    public function showAction(Request $request, $class, $id) {
        $admin = $this->get("coobix.admin." . $class);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($admin->getEntityShortcutName())->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('NO SE PUDO ENCONTRAR LA ENTIDAD.');
        }

        $formOptions = array(
            'id' => $id,
            'label' => 'Borrar'
        );
        $deleteForm = $this->createDeleteForm($admin, $formOptions);

        $template = ($admin->getShowTemplate() != null) ? $admin->getShowTemplate() : $admin->getEntityShortcutName() . ':Admin/show.html.twig';
        
        return $this->render($template, array(
                    'entity' => $entity,
                    'admin' => $admin,
                    'deleteForm' => $deleteForm->createView(),
        ));
    }

    public function listAction(Request $request, $class) {

        $admin = $this->get("coobix.admin." . $class);
        
        $em = $this->getDoctrine()->getManager();

        //CONSULTA PARA RETORNAR LAS ENTIDADES DE LA PAGINA
        //A ESTA CONSULTA LUEGO SE LE AGREGAN FILTROS DE ORDEN, PAGINADO, ETC
        $qb = $em->createQueryBuilder();
        $qb->select('e')->from($admin->getEntityShortcutName(), 'e');
        $qb->orderBy('e.createdAt', 'DESC');

        $list = new BaseList($this->getDoctrine(), $admin->getClass());
        $list->setQb($qb);

        $listSearchForm = $this->createSearchListForm($admin);
        $listSearchForm->handleRequest($request);
        $listSearchFormIsSubmited = false;
        if ($listSearchForm->isSubmitted()) {
            $listSearchFormIsSubmited = true;
            if ($listSearchForm->isValid()) {
                $list->applyFilters();
            }
        }

        $list->applyOrder();


        //PAGINADOR
        //Clono el query builder para el paginador
        $qbp = clone($list->getQb());
        $qbp->select('count(e.id)');
        $entitiesCount = $qbp->getQuery()->getSingleResult();

        $pager = $this->get('coobix.pager');
        //Url en la cual trabaja el paginador
        $listUrlParams = array(
            'routeName' => 'admin_list',
            'parameters' => array('class' => strtolower($admin->getClassName()))
        );
        //LA URL DEBERIA SER PASADA DIRECTAMENTE Y NO PARA QUE LA HAGA
        //EL PAGINADOR
        $pager->setListUrlParams($listUrlParams);
        $pager->setNumItems($entitiesCount[1]);
        $pager->configure();

        $list->applyLimits();
        $list->getResult();

        $template = ($admin->getListTemplate() != null) ? $admin->getListTemplate() : $admin->getClassName() . '/Admin/list.html.twig';

        return $this->render($template, array(
                    'admin' => $admin,
                    'listSearchFormIsSubmited' => $listSearchFormIsSubmited,
                    'listSearchForm' => $listSearchForm->createView(),
                    'list' => $list,
                    'pager' => $pager,
        ));
    }

    protected function createEditForm($admin, $entity) {

        $form = $this->createForm($admin->getEditForm(), $entity, array(
            'action' => $this->generateUrl('admin_update', array('class' => strtolower($admin->getClassName()), 'id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('apply', SubmitType::class, array('label' => 'APLICAR'));
        $form->add('save', SubmitType::class, array('label' => 'MODIFICAR'));

        return $form;
    }

    protected function createCreateForm($admin, $entity) {

        $form = $this->createForm($admin->getNewForm(), $entity, array(
            'action' => $this->generateUrl('admin_create', array('class' => strtolower($admin->getClassName()))),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'CREAR'));

        return $form;
    }

    /**
     * Creates a form to delete a class by id.
     *
     * @param mixed $id The class id
     *
     * @return Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($admin, $formOptions = array()) {

        $fo = array(
            'method' => 'DELETE',
            'id' => 0,
            'label' => ' ',
            'action' => null,
        );

        $o = array_merge($fo, $formOptions);

        if ($o['action'] === null) {
            $o['action'] = $this->generateUrl("admin_delete", array(
                'class' => strtolower($admin->getClassName()),
                'id' => $o['id'],
            ));
        }

        return $this->get('form.factory')->createNamedBuilder('form_' . $o['id'], FormType::class, null)
                        ->setAction($o['action'])
                        ->setMethod($o['method'])
                        ->add('id', HiddenType::class)
                        ->add('submit', SubmitType::class, array('label' => $o['label']))
                        ->getForm();
    }


}
