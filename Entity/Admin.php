<?php

namespace Coobix\AdminBundle\Entity;

/**
 * Admin
 */
abstract class Admin
{

    protected $class;
    protected $rC;
    //LIST
    protected $list;
    protected $listTemplate;
    protected $listSearchForm;
    //NEW
    protected $newForm;
    protected $newTemplate;
    protected $newTitle;
    //NEWBY
    protected $newByForm;
    //EDIT
    protected $editForm;
    protected $editTemplate;
    protected $editTitle;
    //SHOW
    protected $showTemplate;

    public function __construct($class) {
        $this->class = $class;
        $this->rC = new \ReflectionClass($this->class);
    }

    public function setNewForm($newForm) {
        $this->newForm = $newForm;
        return $this;
    }

    public function getNewForm() {
        return $this->newForm;
    }

    public function setNewByForm( $newByForm) {
        $this->newByForm = $newByForm;
        return $this;
    }

    public function getNewByForm() {
        return $this->newByForm;
    }
    
    public function setNewTitle($newTitle) {
        $this->newTitle = $newTitle;
        return $this;
    }

    public function getNewTitle() {
        return ($this->newTitle === null) ? 'Nuevo ' . $this->getClassName() : $this->newTitle;
    }

    public function setNewTemplate($newTemplate) {
        $this->newTemplate = $newTemplate;
        return $this;
    }

    public function getNewTemplate() {
        return $this->newTemplate;
    }

    public function setEditForm( $editForm) {
        $this->editForm = $editForm;
        return $this;
    }

    public function getEditForm() {
        return $this->editForm;
    }
    
    public function setEditTitle($editTitle) {
        $this->editTitle = $editTitle;
        return $this;
    }

    public function getEditTitle() {
        return ($this->editTitle === null) ? 'Editar ' . $this->getClassName() : $this->editTitle;
    }

    public function setEditTemplate($editTemplate) {
        $this->editTemplate = $editTemplate;
        return $this;
    }

    public function getEditTemplate() {
        return $this->editTemplate;
    }

    public function getListSearchForm() {
        return $this->listSearchForm;
    }

    public function setListTemplate($listTemplate) {
        $this->listTemplate = $listTemplate;
        return $this;
    }

    public function getListTemplate() {
        return $this->listTemplate;
    }

    public function setShowTemplate($showTemplate) {
        $this->showTemplate = $showTemplate;
        return $this;
    }

    public function getShowTemplate() {
        return $this->showTemplate;
    }

    public function setListSearchForm( $listSearchForm) {
        $this->listSearchForm = $listSearchForm;

        return $this;
    }

    public function getClass() {
        return $this->class;
    }

    public function getClassName() {
        return $this->rC->getShortName();
    }

    public function getBundleName() {
        $nameSpace = explode("\\", $this->rC->getNamespaceName());
        return (count($nameSpace) == 3) ? $nameSpace[0] . $nameSpace[1] : $nameSpace[0];
    }

    //RETORNA LA URL DEL LISTADO
    public function getListUrl() {
        return $this->listUrl;
    }

    public function getEntityShortcutName() {
        return $this->getBundleName() . ':' . $this->getClassName();
    }

    public function getControllerFullyName() {
        $nameSpace = explode("\\", $this->rC->getNamespaceName());

        //AppBundle\Entity
        if (count($nameSpace) == 2) {
            return $nameSpace[0] . '\\Admin\\Controller\\' . $this->getClassName() . 'AdminController';
        }
        //Vendor\AppBundle\Entity
        return $nameSpace[0] . '\\' . $nameSpace[1] . '\\Admin\\Controller\\' . $this->getClassName() . 'AdminController';
    }
}
